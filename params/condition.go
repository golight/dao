package params

type Condition struct {
	Equal       map[string]interface{}
	NotEqual    map[string]interface{}
	LessThan    map[string]interface{}
	GreaterThan map[string]interface{}
	Order       []*Order
	LimitOffset *LimitOffset
	ForUpdate   bool
	Upsert      bool
}

type Order struct {
	Field string
	Asc   bool
}

type LimitOffset struct {
	Offset int64
	Limit  int64
}
