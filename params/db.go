package params

type DB struct {
	Net      string `env:"DB_NET"`
	Driver   string `env:"DB_DRIVER" env-default:"postgres"`
	Name     string `env:"DB_NAME" env-default:"postgres"`
	User     string `env:"DB_USER" env-default:"postgres"`
	Password string `env:"DB_PASSWORD"`
	Host     string `env:"DB_HOST" env-default:"localhost"`
	MaxConn  int    `env:"DB_MAXCONN" env-default:"8"`
	Port     string `env:"DB_PORT" env-default:"5432"`
	Timeout  int    `env:"DB_TIMEOUT" env-default:"8"`
}
