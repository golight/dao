// Package builder makes SQL queries for data access object
package builder

import (
	"fmt"
	"strings"

	sq "github.com/Masterminds/squirrel"
	"gitlab.com/golight/dao/params"
	"gitlab.com/golight/entity/tabler"
	"gitlab.com/golight/scanner"
)

type BuilderFace interface {
	CreateQuery(table tabler.Tabler, opts ...interface{}) (string, []interface{}, error)
	UpsertQuery(entities []tabler.Tabler, opts ...interface{}) (string, []interface{}, error)
	SelectQuery(tableName string, condition params.Condition, fields ...string) (string, []interface{}, error)
	SelectQueryFields(table tabler.Tabler, condition params.Condition, opts ...interface{}) (string, []interface{}, error)
	UpdateQuery(entity tabler.Tabler, condition params.Condition, operation string, opts ...interface{}) (string, []interface{}, error)
}

// Builder builds SQL queries
type Builder struct {
	sqlBuilder sq.StatementBuilderType // sqlBuilder is a builder of SQL statements.
	Scanner    scanner.Scanner         // scanner is a scanner object to interact with tables.
}

type Field struct {
	name string
}

// NewBuilder constructs a new Builder object
func NewBuilder(dbConf params.DB, scanner scanner.Scanner) *Builder {
	var builder sq.StatementBuilderType
	if dbConf.Driver != "mysql" {
		builder = sq.StatementBuilder.PlaceholderFormat(sq.Dollar)
	}

	return &Builder{sqlBuilder: builder, Scanner: scanner}
}

// getFields returns field names and their values (pointers).
func (b *Builder) getFields(entity tabler.Tabler, operation string, fieldsOpts []*Field) ([]string, []interface{}) {
	fields := b.Scanner.OperationFields(entity, operation)
	var fieldsPointers []interface{}
	var fieldsName []string
	for i := range fields {
		if len(fieldsOpts) == 0 || checkFieldContains(fields[i], fieldsOpts) {
			fieldsPointers = append(fieldsPointers, fields[i].Pointer)
			fieldsName = append(fieldsName, fields[i].Name)
		}
	}

	return fieldsName, fieldsPointers
}

// checkFieldContains checks whether field contains in fieldsOpts or not
func checkFieldContains(fieldScanner *scanner.Field, fieldsOpts []*Field) bool {
	for _, field := range fieldsOpts {
		if field.name == fieldScanner.Name {
			return true
		}
	}
	return false
}

// getFieldsFromOpts returns fields to interact with
func getFieldsFromOpts(opts ...interface{}) []*Field {
	var fields []*Field
	for _, opt := range opts {
		switch field := opt.(type) {
		case string:
			fields = append(fields, &Field{name: field})
		}
	}
	return fields
}

// CreateQuery constructs a INSERT SQL query.
func (b *Builder) CreateQuery(table tabler.Tabler, opts ...interface{}) (string, []interface{}, error) {
	fields := getFieldsFromOpts(opts)
	createFields, createFieldsPointers := b.getFields(table, scanner.Create, fields)

	queryRaw := b.sqlBuilder.Insert(table.TableName()).Columns(createFields...).Values(createFieldsPointers...)

	return queryRaw.ToSql()
}

// UpsertQuery constructs a INSERT SQL query with update.
func (b *Builder) UpsertQuery(entities []tabler.Tabler, opts ...interface{}) (string, []interface{}, error) {
	fields := getFieldsFromOpts(opts)
	createFields, _ := b.getFields(entities[0], scanner.Create, fields)
	queryRaw := b.sqlBuilder.Insert(entities[0].TableName()).Columns(createFields...)

	for i := range entities {
		_, createFieldsPointers := b.getFields(entities[i], "create", fields)
		queryRaw = queryRaw.Values(createFieldsPointers...)
	}

	query, args, err := queryRaw.ToSql()
	if err != nil {
		return "", nil, err
	}

	conflictFields, _ := b.getFields(entities[0], scanner.Conflict, fields)
	if len(conflictFields) > 0 {
		query = query + " ON CONFLICT (%s)"
		query = fmt.Sprintf(query, strings.Join(conflictFields, ","))
		query = query + " DO UPDATE SET"
	}
	upsertFields, _ := b.getFields(entities[0], scanner.Upsert, fields)
	for _, field := range upsertFields {
		query += fmt.Sprintf(" %s = excluded.%s,", field, field)
	}
	if len(upsertFields) > 0 {
		query = query[0 : len(query)-1]
	}
	return query, args, nil
}

// SelectQuery constructs a SELECT SQL query.
func (b *Builder) SelectQuery(tableName string, condition params.Condition, fields ...string) (string, []interface{}, error) {
	if condition.ForUpdate {
		temp := []string{"FOR UPDATE"}
		temp = append(temp, fields...)
		fields = temp
	}

	var queryRaw sq.SelectBuilder
	if len(fields) > 0 {
		queryRaw = b.sqlBuilder.Select(fields...).From(tableName)
	} else {
		queryRaw = b.sqlBuilder.Select("*").From(tableName)
	}

	if condition.Equal != nil {
		for field, args := range condition.Equal {
			queryRaw = queryRaw.Where(sq.Eq{field: args})
		}
	}

	if condition.NotEqual != nil {
		for field, args := range condition.NotEqual {
			queryRaw = queryRaw.Where(sq.NotEq{field: args})
		}
	}

	if condition.LessThan != nil {
		for field, args := range condition.LessThan {
			queryRaw = queryRaw.Where(sq.Lt{field: args})
		}
	}

	if condition.GreaterThan != nil {
		for field, args := range condition.GreaterThan {
			queryRaw = queryRaw.Where(sq.Gt{field: args})
		}
	}

	if condition.Order != nil {
		for _, order := range condition.Order {
			direction := "DESC"
			if order.Asc {
				direction = "ASC"
			}
			queryRaw = queryRaw.OrderBy(fmt.Sprintf("%s %s", order.Field, direction))
		}
	}

	if condition.LimitOffset != nil {
		if condition.LimitOffset.Limit > 0 {
			queryRaw.Limit(uint64(condition.LimitOffset.Limit))
		}
		if condition.LimitOffset.Offset > 0 {
			queryRaw.Offset(uint64(condition.LimitOffset.Offset))
		}
	}
	return queryRaw.ToSql()
}

// SelectQueryFields constructs a SELECT SQL query with all fields.
func (b *Builder) SelectQueryFields(table tabler.Tabler, condition params.Condition, opts ...interface{}) (string, []interface{}, error) {
	fieldsOpts := getFieldsFromOpts(opts)
	fields, _ := b.getFields(table, scanner.AllFields, fieldsOpts)
	return b.SelectQuery(table.TableName(), condition, fields...)
}

// UpdateQuery constructs a UPDATE SQL query.
func (b *Builder) UpdateQuery(entity tabler.Tabler, condition params.Condition, operation string, opts ...interface{}) (string, []interface{}, error) {
	fields := getFieldsFromOpts(opts)
	updateFields, updateFieldsPointers := b.getFields(entity, operation, fields)

	updateRaw := b.sqlBuilder.Update(entity.TableName())

	if condition.Equal != nil {
		for field, args := range condition.Equal {
			updateRaw = updateRaw.Where(sq.Eq{field: args})
		}
	}

	if condition.NotEqual != nil {
		for field, args := range condition.NotEqual {
			updateRaw = updateRaw.Where(sq.NotEq{field: args})
		}
	}

	for i := range updateFields {
		updateRaw = updateRaw.Set(updateFields[i], updateFieldsPointers[i])
	}

	return updateRaw.ToSql()
}
