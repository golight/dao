package main

import (
	"context"
	"fmt"
	"log"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq" // Используйте соответствующий драйвер для вашей базы данных
	"gitlab.com/golight/dao"
	"gitlab.com/golight/dao/params"
	"gitlab.com/golight/scanner"
)

type MockTabler struct {
	table *scanner.Table `db:"postgres"`
}

func (m *MockTabler) TableName() string {
	return m.table.Name
}

func (m *MockTabler) OnCreate() []string {
	return []string{"field1", "field2"}
}

func (m *MockTabler) FieldsPointers() []interface{} {
	return []interface{}{nil, nil}
}

func main() {
	// Обратите внимание, что вам нужно будет заменить "user=postgres password=secret dbname=postgres sslmode=disable" на строку подключения к вашей базе данных.
	// Создайте подключение к базе данных
	db, err := sqlx.Connect("postgres", "user=postgres password=secret dbname=postgres sslmode=disable")
	if err != nil {
		log.Fatalln(err)
	}

	// Создайте экземпляр сканера
	s := scanner.NewTableScanner()

	// Создайте экземпляр DAO
	d := dao.NewDAO(db, params.DB{Driver: "postgres"}, s)

	// Используйте методы DAO
	err = d.Ping(context.Background())
	if err != nil {
		log.Fatalln(err)
	}

	fmt.Println("Ping successful")

	// Здесь вы можете использовать другие методы DAO, такие как Create, Upsert и т.д.
	// Например, d.Create(context.Background(), ваша_сущность)
	tabler := &MockTabler{table: &scanner.Table{
		Name: "users",
		Fields: []*scanner.Field{{
			IDx:        0,
			Name:       "field1",
			Type:       "INTEGER",
			Default:    "0",
			Constraint: scanner.Constraint{},
			Table:      &scanner.Table{},
			Pointer:    nil,
		}},
		FieldsMap:   map[string]*scanner.Field{},
		Constraints: []scanner.Constraint{},
		OperationFields: map[string][]*scanner.Field{
			"create": {&scanner.Field{
				Name:       "field1",
				Pointer:    nil,
			}},
		},
		Entity: nil,
	}}
	tabler.table.Entity = tabler
	tabler.table.Fields[0].Table = tabler.table
	s.RegisterTable(tabler)
	log.Print(s.Table("users").Entity)
	d.Create(context.Background(), s.Table("users").Entity)
	d.Create(context.Background(), s.Table("users").Entity)
	var result interface{}
	d.List(context.Background(), result, tabler, params.Condition{
		LessThan:    map[string]interface{}{"field1": 1},
		GreaterThan: map[string]interface{}{"field1": -1},
		Equal:       map[string]interface{}{"field1": 0},
	})
	log.Print(result)
}
