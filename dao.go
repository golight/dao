// Package dao provides a data access object for a database.
package dao

import (
	"context"
	"database/sql"
	"fmt"

	"gitlab.com/golight/dao/params"
	"gitlab.com/golight/dao/sql/builder"
	"gitlab.com/golight/entity/tabler"
	"gitlab.com/golight/scanner"

	"github.com/jmoiron/sqlx"
)

const (
	DriverMysql    = "mysql"
	DriverPostgres = "postgres"
	DriverSqlite3  = "sqlite3"
	DriverRamsql   = "ramsql"
)

//go:generate mockgen -source=./dao.go -destination=./mock/dao_mock.go -package=mock
type DAOFace interface {
	Begin() (*sqlx.Tx, error)
	Ping(ctx context.Context) error
	Create(ctx context.Context, entity tabler.Tabler, opts ...interface{}) (int64, error)
	Upsert(ctx context.Context, entities []tabler.Tabler, opts ...interface{}) error
	GetCount(ctx context.Context, entity tabler.Tabler, condition params.Condition, opts ...interface{}) (uint64, error)
	List(ctx context.Context, dest interface{}, table tabler.Tabler, condition params.Condition, opts ...interface{}) error
	Update(ctx context.Context, entity tabler.Tabler, condition params.Condition, operation string, opts ...interface{}) error
	Exec(ctx context.Context, query string, tx *sqlx.Tx, args ...interface{}) (sql.Result, error)
	LockTable(ctx context.Context, tableName string, mode string, tx *sqlx.Tx) error
}

// DAO is a data access object.
type DAO struct {
	db               *sqlx.DB         // db refers to a database.
	statementBuilder *builder.Builder // builder is a builder of SQL statements.
}

// NewDAO creates a new data access object.
func NewDAO(db *sqlx.DB, dbConf params.DB, scanner scanner.Scanner) *DAO {
	return &DAO{db: db, statementBuilder: builder.NewBuilder(dbConf, scanner)}
}

// Begin starts a transaction.
func (s *DAO) Begin() (*sqlx.Tx, error) {
	return s.db.Beginx()
}

// Exec executes the specified query.
func (s *DAO) Exec(ctx context.Context, query string, tx *sqlx.Tx, args ...interface{}) (sql.Result, error) {
	if tx != nil {
		return tx.ExecContext(ctx, query, args...)
	}
	return s.db.ExecContext(ctx, query, args...)
}

// LockTable locks a table for writing.
func (s *DAO) LockTable(ctx context.Context, tableName string, mode string, tx *sqlx.Tx) error {
	query := fmt.Sprintf("LOCK TABLE %s IN %s MODE", tableName, mode)
	_, err := s.Exec(ctx, query, tx)

	return err
}

// Ping verifies that the database connection is still alive.
func (s *DAO) Ping(ctx context.Context) error {
	return s.db.PingContext(ctx)
}

// Create creates a new field in the database and returns the index of this field.
func (s *DAO) Create(ctx context.Context, table tabler.Tabler, opts ...interface{}) (int64, error) {
	query, args, err := s.statementBuilder.CreateQuery(table, opts)
	if err != nil {
		return -1, err
	}

	var result sql.Result
	if tx := getTransaction(opts...); tx != nil {
		result, err = tx.ExecContext(ctx, query, args...)
	} else {
		result, err = s.db.ExecContext(ctx, query, args...)
	}

	if err != nil {
		return -1, err
	}
	id, err := result.LastInsertId()
	if err != nil {
		return -1, err
	}
	return id, nil
}

// Upsert updates an existing row if a specified value already exists in a table,
// and inserts a new row if the specified value doesn't already exist.
func (s *DAO) Upsert(ctx context.Context, entities []tabler.Tabler, opts ...interface{}) error {
	if len(entities) < 1 {
		return fmt.Errorf("SQL adapter: zero entities passed")
	}

	query, args, err := s.statementBuilder.UpsertQuery(entities, opts)
	if err != nil {
		return err
	}

	if tx := getTransaction(opts...); tx != nil {
		_, err = tx.ExecContext(ctx, query, args...)
	} else {
		_, err = s.db.ExecContext(ctx, query, args...)
	}

	return err
}

// GetCount returns the number of rows that match a specified criterion.
func (s *DAO) GetCount(ctx context.Context, entity tabler.Tabler, condition params.Condition, opts ...interface{}) (uint64, error) {
	query, args, err := s.statementBuilder.SelectQuery(entity.TableName(), condition, "COUNT(*)")
	if err != nil {
		return 0, err
	}

	var rows *sqlx.Rows
	if tx := getTransaction(opts...); tx != nil {
		rows, err = tx.QueryxContext(ctx, query, args...)
	} else {
		rows, err = s.db.QueryxContext(ctx, query, args...)
	}

	if err != nil {
		return 0, err
	}

	var count uint64
	// iterate over each row
	for rows.Next() {
		err = rows.Scan(&count)
		if err != nil {
			return 0, err
		}
	}
	// check the error from rows
	err = rows.Err()

	return count, err
}

// List combines the values of a table column from multiple rows into a single comma-separated list of values.
func (s *DAO) List(ctx context.Context, dest interface{}, table tabler.Tabler, condition params.Condition, opts ...interface{}) error {
	query, args, err := s.statementBuilder.SelectQueryFields(table, condition, opts...)
	if err != nil {
		return err
	}

	if tx := getTransaction(opts...); tx != nil {
		err = tx.SelectContext(ctx, dest, query, args...)
	} else {
		err = s.db.SelectContext(ctx, dest, query, args...)
	}

	return err
}

// Update modifies the existing records in a table.
func (s *DAO) Update(ctx context.Context, entity tabler.Tabler, condition params.Condition, operation string, opts ...interface{}) error {
	query, args, err := s.statementBuilder.UpdateQuery(entity, condition, operation, opts...)
	if err != nil {
		return err
	}

	var res sql.Result
	if tx := getTransaction(opts...); tx != nil {
		res, err = tx.ExecContext(ctx, query, args...)
	} else {
		res, err = s.db.ExecContext(ctx, query, args...)
	}
	if err != nil {
		return err
	}
	_, err = res.RowsAffected()

	return err
}

// getTransaction returns a transaction.
func getTransaction(opts ...interface{}) *sqlx.Tx {
	if len(opts) > 0 {
		if tx, ok := opts[0].(*sqlx.Tx); ok {
			return tx
		}
	}
	for _, opt := range opts {
		switch tx := opt.(type) {
		case *sqlx.Tx:
			return tx
		}
	}
	return nil
}
